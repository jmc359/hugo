![a relative link](IMG_0820.jpg)

Hi! My name is Joe Connolly. I am a graduate of the Yale Class of 2022 with a BS in Computer Science. I am particularly interested in the intersection between computer science and robotics.
I worked as part of the [Interactive Machines Group](https://interactive-machines.gitlab.io) 
conducting research in the field of Human-Robot Interaction. Outside of academia, interests of mine 
include playing the piano, watching movies, playing video games, and playing board games.
Please feel free to explore and learn more about what I do!
