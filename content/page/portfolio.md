---
title: Portfolio
subtitle: Here is my portfolio of cutting-edge work
bigimg: [{src: "/hugo/img/baxter.jpg", desc: "Baxter, Sphero, and Cozmos"}]
comments: false
---

## Human-Robot Interaction Research at Yale Interactive Machines Group
![Cozmo](https://gitlab.com/jmc359/hugo/raw/master/content/page/cozmo.png)

I work with Professor Marynel Vazquez as part of the Yale Interactive Machines Group. 
We recently researched the emergence of social influence within group HRI, focusing on how emotional responses can be used to prompt human interventions in response to robot abuse. 
I am currently working with graph neural networks to predict future human social engagement for human-computer and human-robot interactive systems. \[Relevant repositories: [Robot Abuse Study Code](https://gitlab.com/interactive-machines/robot_abuse), [Generating F-Formations Study Code](https://gitlab.com/interactive-machines/spatial_behavior/genff), [Anki Cozmo Driver](https://gitlab.com/interactive-machines/cozmo_driver)\]

- **Robot Abuse Study, Lead Author/Developer, August 2018-March 2020**
    - I led a year-long study to help understand how robots’ reactions to robot mistreatment around them can help nearby humans intervene and restore mutual prosocial behavior within a group.
    - I programmed a multi-robot system with various on-board robotic sensors and a Kinect, trained a neural network, coordinated study times for participants, ran experiment sessions, conducted thorough data analysis, and wrote most of the manuscript for the experiment. My work under Professor Brian Scassellati and Professor Marynel Vazquez culminated in a full user study accepted at the HRI 2020 robotics conference.


- **Yale Robots for Good, Lead Developer, January 2020-September 2020**
    - I led a student team in writing and testing a mobile iOS and Android application that helped kids play with one another through their Vector robots during the social isolation of the COVID-19 pandemic.
    - I helped convince Yale to send Vector robots to kids from lower-income families in the New Haven area such that they could play remotely with each other given the quarantine restrictions in place at their schools.
    - I worked under Professor Brian Scassellati and Professor Marynel Vazquez to complete this project, which resulted in a full paper accepted to HRI 2021.


- **Robot Embodiment and Group Membership, Lead Author/Developer, November 2020-February 2020**
    - I constructed an online user-study during the COVID-19 pandemic to study how robot embodiment influences how humans perceive their group conversational membership in social contexts.
    - I helped design the study, constructed the visual simulations in Unity for study images, and conducted thorough data analysis to arrive at sound scientific conclusions.
    - I worked under Professor Marynel Vazquez to complete this project, which resulted in a late-breaking report accepted to HRI 2021.


- **Pose Generation for Social Robots, Developer, August 2020-December 2020**
    - I implemented a generative adversarial network to improve how mobile robots in our lab join conversational groups in a socially intelligent manner.
    - I worked under Professor Marynel Vazquez to complete this project, which resulted in a full journal paper accepted to Frontiers in AI and Robotics.


- **Automatic Lighting for Photography Robot, Lead Developer, August 2019-December 2019**
    - I implemented a reinforcement learning deep neural network to help our photography robot Shutter find the best lighting in which to take photos using programmable light bulbs. 
    - I worked under Professor Marynel Vazquez to complete this project.
<br><br>

---

## Sofware Development at Innovative Systems, Inc.
![Innovative Systems, Inc.](https://gitlab.com/jmc359/hugo/raw/master/content/page/innovsystems.jpeg)

- **Innovative Data Profiler, Lead Software Engineer, January 2021-June 2021**
    - I accomplished 10x or faster computation speed (while simultaneously computing more statistical parameters and
transformations) by implementing a new parallelized backend for data analysis in C# .NET for an original statistical graphing and summarization software to generate profits for Innovative Systems. 
    - I built dynamic visualizations and clearer user flow for the graphical user interface that incorporated user feedback,
resulting in improved user experiences. 
    - I led multiple interns on our team, both in discussing new features and setting timetables for implementing new code
and fixing reported bugs, resulting in a product that was ready for productization on time.
<br><br>

---

## Robotics Research at CMU Transportation, Bots, and Disability Lab (Robotics Institute)
![Podi](https://gitlab.com/jmc359/hugo/raw/master/content/page/Podi.png)

Over summers 2018 and 2019, I conducted research for the CMU Transportation, Bots, and Disability Lab, where I worked 
on a number of software projects for the Baxter and Pioneer 3-DX robots. These projects included applications for safety, 
facial recognition, path planning, path execution, and localization. Specifically, for the Pioneer 3-DX (above), 
I helped implement a path planning and execution simulation environment in the RViz platform for repeated testing of a geometric 
model of a human following behind the robot. My work is part of a paper (["Follow the Robot: Modeling Coupled Human-Robot Dyads During Navigation"](https://ras.papercept.net/conferences/scripts/rtf/IROS19_ContentListWeb_3.html)) that will be published at the 2019 proceedings of the IEEE/RSJ International Conference on
Intelligent Robots and Systems. \[Relevant repositories: [Pioneer 3-DX Simulator](https://github.com/CMU-ARM/p3dx_2dnav_simulator/tree/joe-dev/p3dx_2dnav), [Baxter Web Interface](https://github.com/CMU-ARM/baxter_web_inteface), [Baxter Safety](https://github.com/CMU-ARM/lab_baxter_safety)\]

- **Coupled Planner for Assistive Navigation, Developer, Summer 2018**
    - I programmed the Pioneer 3DX robot to incorporate our original coupled navigation planner for assisting visually-impaired people.
    - I ported the planner for Robot Operating System simulations in Gazebo to test the efficacy of the planner in unknown complex indoor environments.
    - I worked under Professor Aaron Steinfeld to complete this project, which resulted in a full paper accepted to the IROS 2019 conference.

- **Baxter Guide in CMU Lobby, Lead Developer, Summer 2019**
    - I built the software infrastructure to deploy a social robot Baxter as a guide in the lobby of CMU’s Robotics Institute.
    - I implemented a speech-recognizing, responsive system that would help visitors with tasks such as querying the next bus arrival time for a particular route or providing room directions.
    - I worked under Professor Aaron Steinfeld to complete this project.
<br><br>

---

## Leap@CMU Final Project
![RCube](https://gitlab.com/jmc359/hugo/raw/master/content/page/RCube.png)

In summer 2016, I learned undergraduate-level computer science in high school and built a robot that solves Rubik’s cubes programmed in C++ and Java. [Here’s the robot in action](https://www.youtube.com/watch?v=oHNqW97JLAo)
