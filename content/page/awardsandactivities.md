---
title: Awards & Activities
subtitle: Here are my awards and activities
bigimg: [{src: "/hugo/img/fullsizeoutput_45.jpeg", desc: "CS50"}]
comments: false
---
## Awards
### CRA Outstanding Undergraduate Research Award Finalist, December 2021
I was selected as a finalist for the prestigious [Computing Research Association’s (CRA) Outstanding Undergraduate Researcher Award](https://cra.org/about/awards/outstanding-undergraduate-researcher-award/). This award recognizes the world's top student researchers in the field of computer science. 

### HRI Best Paper Nominee, March 2021
My paper for the Yale Robots for Good Project (“Challenges Deploying Robots During a Pandemic: An Effort to Fight Social Isolation Among Children”) was nominated as a best paper candidate for HRI 2021.

### HRI Pioneers Award, March 2020
I was in this cohort of the world's top student researchers, which provides the opportunity for students to present and discuss their work with distinguished student peers and senior scholars in the field of Human-Robot Interaction.


----

<br><br>
## Activities
### Yale Interactive Machines Group
I am an active member of this robotics research group, with my current focus
being Human-Robot Interaction and the intricacies of human-robot
collaboration.

### Yale HRI Reading Groups
I have been a member of multiple HRI reading groups of graduate students and professors. 
We discuss the latest papers, quantitative methods, and techniques in the field of HRI.

### CS50 Teaching Staff
I taught review sessions, conducted office hours, and hosted individual help sessions for students taking the largest introductory computer science course at Yale to help inspire the next generation of aspiring computer scientists.
        	
