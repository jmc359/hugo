---
title: Classes
subtitle: Here are the technical classes I have taken to date
bigimg: [{src: "/hugo/img/Binary.png", desc: "Data Clusters"}]
comments: false
---
## Spring 2022
**CS 433** Computer Networks

## Fall 2021
**CS 453** Unsupervised Learning for Big Data   
**CS 490** Senior Project

## Spring 2020
**CS 421** Compilers & Interpreters   
**CS 473** Intelligent Robotics Laboratory  
**CS 478** Computer Graphics    
**S&DS 230** Data Exploration and Analysis

## Fall 2019
**CS 459** Building Interactive Machines  
**CS 472** Intelligent Robotics   
**S&DS 365** Data Mining & Machine Learning   

## Spring 2019      
**CS 365** Algorithms 	                                    
**CS 429** Introduction to Human-Computer Interaction   
**MATH 244** Discrete Mathematics  

## Fall 2018
**CS 290** Directed Research    
**CS 323** Systems Programming & Computer Organization    

## Spring 2018
**CS 223** Data Structures & Programming Techniques 	    
**EENG 201** Introduction to Computer Engineering 	        
**MATH 225** Linear Algebra & Matrix Theory 

## Fall 2017
**CS 50** Introduction to Computer Science 	                
**ENAS 151** Multivariable Calculus for Engineers 
