---
title: Publications
subtitle: Here are my robotics publications
bigimg: []
comments: false
---

- **J. Connolly**, V. Mocz, N. Salomons, J. Valdez, N. Tsoi, B. Scassellati, and M. Vázquez. 2020. Prompting Prosocial Human Interventions in Response to Robot Mistreatment. In *Proceedings of the 2020 ACM/IEEE International Conference on Human-Robot Interaction (HRI ’20)*, March 23–26, 2020, Cambridge, United Kingdom. ACM, New York, NY, USA, 10 pages. [PDF](https://interactive-machines.gitlab.io/assets/papers/connolly-HRI20.pdf) [Project Website](https://interactive-machines.gitlab.io/projects/prosocial_interventions)

- **J. Connolly**, N. Tsoi, and M. Vázquez. “Perceptions of Conversational Group Membership based on Robots' Spatial Positioning: Effects of Embodiment” *Companion of the 2021 ACM/IEEE International Conference on Human-Robot Interaction (HRI '21 Companion)* [PDF](https://interactive-machines.gitlab.io/assets/papers/connolly-HRI21.pdf) [Project Website](https://interactive-machines.com/projects/spatial)

- N. Tsoi, **J. Connolly**, E. Adéníran, A. Hansen, K.T. Pineda, T. Adamson, S. Thompson, R. Ramnauth, M. Vázquez, and B. Scassellati. “Challenges Deploying Robots During a Pandemic: An Effort to Fight Social Isolation Among Children.” *Proceedings of the 2021 ACM/IEEE International Conference on Human-Robot Interaction (HRI '21)* [PDF](https://interactive-machines.gitlab.io/assets/papers/tsoi-HRI21.pdf) [Project Website](https://robotsforgood.yale.edu/)

- A. Nanavati, X. Z. Tan, **J. Connolly** and A. Steinfeld, "Follow The Robot: Modeling Coupled Human-Robot Dyads During Navigation," *2019 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)*, Macau, China, 2019, pp. 3836-3843. [PDF](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8967656&isnumber=8967518)

- M. Vázquez, A. Lew, E. Gorevoy, and **J. Connolly**. "Pose Generation for Social Robots in Conversational Group Formations." *2021 Frontiers in Robotics and AI*. [PDF](https://interactive-machines.com/assets/papers/mvazquez-frontiers22.pdf) [Demo](https://anna.cs.yale.edu:9995)

- **J. Connolly**. Preventing robot abuse through emotional robot responses. In *Companion of the 2020 ACM/IEEE International Conference on Human-Robot Interaction*, pages 558–560. [PDF](https://dl.acm.org/doi/pdf/10.1145/3371382.3377433)
